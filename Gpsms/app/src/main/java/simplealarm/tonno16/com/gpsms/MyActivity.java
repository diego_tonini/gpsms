package simplealarm.tonno16.com.gpsms;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.Calendar;


public class MyActivity extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    public SimpleCursorAdapter adapter;
    private MyDatabase db;
    private ListView listViewContacts;
    private CustomTextView textViewAdd,textViewOn,textViewOff;

    private SeekBar seekInterval;
    static final int PICK_CONTACT_REQUEST = 1;  // The request code for pick contact
    static final int SWITCH_GPS_REQUEST = 2;
    private String numero="-1";
    public static Context mContext;
    private int INTERVAL_MINUTES = 5; // 10 min default;

    protected static int ID_ALARM = 666;
    private boolean isAlarmSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        db=new MyDatabase(getApplicationContext());
        mContext = getApplicationContext();


        listViewContacts = (ListView) findViewById(R.id.listViewContacts);
        textViewAdd = (CustomTextView) findViewById(R.id.textViewAdd);
        textViewAdd.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(isMyAlarmSet()){
                    Toast.makeText(mContext,"attiva",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext,"NON attiva",Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        textViewAdd.setOnClickListener(this);
        textViewOn = (CustomTextView) findViewById(R.id.textViewOn);
        textViewOn.setOnClickListener(this);
        textViewOff = (CustomTextView) findViewById(R.id.textViewOff);
        textViewOff.setOnClickListener(this);
        seekInterval = (SeekBar) findViewById(R.id.seekInterval);
        seekInterval.setOnSeekBarChangeListener(this);
        seekInterval.setProgress(0);


      Cursor cursor = db.fetchProducts();

       adapter=new SimpleCursorAdapter( //semplice adapter per i cursor
                this,
                R.layout.row_contact, //il layout di ogni riga/prodotto
                cursor,
                new String[]{MyDatabase.ProductsMetaData.PRODUCT_NAME_KEY,MyDatabase.ProductsMetaData.PRODUCT_PRICE_KEY},//questi colonne
                new int[]{R.id.textViewRowName,R.id.textViewRowNumber});//in queste views



       listViewContacts.setAdapter(adapter);

        listViewContacts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
                int INDEX_NAME = 1;
                int INDEX_NUMBER = 2;
                Cursor cursor = (Cursor) adapterView.getItemAtPosition(pos);
                db.deleteContact(cursor.getString(INDEX_NAME),cursor.getString(INDEX_NUMBER));
                adapter.changeCursor(db.fetchProducts());
                return true;
            }
        });

        // automatic emtpy listview
        CustomTextView textViewEmpty = (CustomTextView) findViewById(R.id.textViewEmpty);
        listViewContacts.setEmptyView(textViewEmpty);




    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles

    }

    private boolean isGpsEnable(){
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void showAlertGps(){
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle("Please switch on Gps locationn for a best use");
            adb.setPositiveButton("Active",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            adb.setNegativeButton("Decline",null)
            .setCancelable(false)
            .show();
    }

    public static Context getmContext(){
       return mContext;
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(Phone.CONTENT_TYPE); // Show user only contacts w/ phone numbers
        startActivityForResult(pickContactIntent, PICK_CONTACT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request it is that we're responding to
        if (requestCode == PICK_CONTACT_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // Get the URI that points to the selected contact
                Uri contactUri = data.getData();
                // We only need the NUMBER column, because there will be only one row in the result
                String[] projection = {Phone.NUMBER,Phone.DISPLAY_NAME};

                // Perform the query on the contact to get the NUMBER column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getContentResolver()
                        .query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int columnNumber = cursor.getColumnIndex(Phone.NUMBER);
                String number = cursor.getString(columnNumber);
                int columnName = cursor.getColumnIndex(Phone.DISPLAY_NAME);
                String name = cursor.getString(columnName);

                db.insertProduct(name, number);
                Toast.makeText(mContext,name+" - "+number+" added to list",Toast.LENGTH_SHORT).show();
                adapter.changeCursor(db.fetchProducts());

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){

            case R.id.itemRateMe :
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;

            case R.id.itemAsk :
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[] { "multivoltage@gmail.com" });
                email.putExtra(Intent.EXTRA_SUBJECT, "GPSMS - Question");
                email.putExtra(Intent.EXTRA_TEXT, "I contato you for...");

                // need this to prompts email client only
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Choose an Email client"));
                break;

            case R.id.itemHelp :
                startActivity(new Intent(this,AboutActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.textViewAdd : pickContact();
                break;

            case R.id.textViewOn :
                startAlarm();
                break;

            case R.id.textViewOff :
                stopAlarm();
                break;

        }
    }

    private void startAlarm(){
        Calendar now = Calendar.getInstance();
        long triggerAtTime = now.getTimeInMillis()+ (1*60*1000);   // 1 minute(1 * 60 * 1000);
        long repeat_alarm_every = (INTERVAL_MINUTES*60*1000);                // repeat every 5 minutes

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(mContext, MyReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, ID_ALARM,  intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerAtTime, repeat_alarm_every, pendingIntent);

        // fermo il servizio per sicurezza
        mContext.stopService(new Intent(mContext,MyService.class));
        showNotification();
        Toast.makeText(mContext,"Start tracking",Toast.LENGTH_SHORT).show();
    }
    private void stopAlarm(){
        try {
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            Intent intent = new Intent(mContext, MyReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, ID_ALARM,  intent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            isAlarmSet=false;
            Toast.makeText(mContext,"Stop tracking",Toast.LENGTH_SHORT).show();
            cancelNotification();
        } catch (Exception e) {
            Log.e("alarm", "AlarmManager update was not canceled. " + e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }


        return false;
    }

    private boolean isMyAlarmSet(){

        boolean alarmUp = (PendingIntent.getBroadcast(MyActivity.this, ID_ALARM,
                new Intent(MyActivity.this, MyReceiver.class)
                        .setAction(getPackageName()+"simplealarm.tonno16.com.gpsms.MY_GPS_INTENT"),
                PendingIntent.FLAG_NO_CREATE) != null);
        return alarmUp;
    }

    private void showNotification(){

       /* Intent notificationIntent = new Intent(mContext, MyActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mContext,0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Resources res = mContext.getResources();
        Notification.Builder builder = new Notification.Builder(mContext);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.icon_app72)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("GPSMS")
                .setContentText("tracking on")
                .build();
        Notification n = builder.getNotification();

        n.defaults |= Notification.DEFAULT_ALL;
        nm.notify(10, n);*/
    }

    private void cancelNotification(){

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(10);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int value = seekBar.getProgress();
        if(value<5){
            INTERVAL_MINUTES = 5;
        } else {
            INTERVAL_MINUTES = value;
        }

        Toast.makeText(getApplicationContext(),"Please restart service",Toast.LENGTH_SHORT).show();
    }
}
