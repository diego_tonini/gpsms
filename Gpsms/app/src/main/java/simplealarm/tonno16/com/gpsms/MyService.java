package simplealarm.tonno16.com.gpsms;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public  final class MyService extends Service{

    private MyDatabase myDatabase;

    // for LOCATION
    private static final String TAG = "gpsms_service";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 1f;
    private LocationListener mLocationListeners = new LocationListener(LocationManager.GPS_PROVIDER);

    //for timer
    private int TIMER_SECONDS = 8; // secondi per inviare tutti i sms.

    private class LocationListener implements android.location.LocationListener{
        Location mLastLocation;
        public LocationListener(String provider)
        {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }
        @Override
        public void onLocationChanged(Location location)
        {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);

            sendSms(mLastLocation);
        }
        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }
        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // We want this service to continue running until it is explicitly

        Log.i("service","onStartCommand()");
        myDatabase = new MyDatabase(getApplicationContext());

        initializeLocationManager();

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }

        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy(){
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            try {
                mLocationManager.removeUpdates(mLocationListeners);
            } catch (Exception ex) {
                Log.i(TAG, "fail to remove location listners, ignore", ex);
            }
        }
    }


    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private void sendSms(Location location){


        Calendar cal = Calendar.getInstance();
        String hh = String.valueOf(cal.get(Calendar.HOUR));
        String mm = String.valueOf(cal.get(Calendar.MINUTE));
        String msg = "Time: "+hh+":"+mm+"\n";
        msg += "https://www.google.com.au/maps/preview/@"+location.getLatitude()+","+location.getLongitude()+","+"10"+"z";

        SmsManager smsManager = SmsManager.getDefault();
        for(String numero : myDatabase.getNumberList()){
            smsManager.sendTextMessage(numero, null, msg, null,null);
        }

        startTimer();
    }

    private void startTimer(){

        Timer t = new Timer();

        TimerTask task = new TimerTask() {

            @Override
            public void run() {
               if (TIMER_SECONDS > 0)
                   TIMER_SECONDS --;
               else {
                   stopSelf();
               }

            }
        };

        t.scheduleAtFixedRate(task, 0, 1000);

    }


}